package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"hellobeego/models"
	"hellobeego/utils"
)

type UserController struct {
	beego.Controller
}

func (this *UserController) Get() {
	user := new(models.User{1, "Messi"})
	token, err := utils.GenerateToken(&user, 0)

	if err != nil {
		fmt.Println(err)
	} else {
		this.Ctx.WriteString(token)
		fmt.Println(token)
	}
}

func (this *UserController) Check() {
	token := ""
	info, err := utils.ValidateToken(token)

	if err != nil {
		this.Ctx.WriteString(err.Error())
		this.StopRun()
	}

	fmt.Println(info)
	this.Ctx.WriteString("success")
}
