package main

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	_ "hellobeego/routers"
)

func main() {
	logs.SetLevel(beego.LevelInformational)
	logs.SetLogger("file", `{"filename":"logs/test.log"}`)

	beego.Run()
}
