package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

type Page struct {
	Id      int
	Website string
	Email   string
}

func init() {
	orm.RegisterDataBase("default", "mysql", "root:1234@tcp(127.0.0.1:3306)/test?charset=utf8")
	orm.RegisterModel(new(Page))
}

func GetPage() Page {
	//page := Page{Website: "hellobeego.com", Email: "Messi.qp711@gmail.com"}
	//page := new(Page)
	//page.Website = "hellobeego.com"
	//page.Email = "Messi.qp711@gmail.com"
	//return page

	o := orm.NewOrm()
	page := Page{Id: 1}
	err := o.Read(&page)

	if err != nil {
		fmt.Println("err: ", err.Error())
		return page
	}

	return page
}

func UpdatePage() {
	page := Page{Id: 1, Email: "12345@gmail.com"}
	o := orm.NewOrm()
	_, err := o.Update(&page, "Email")

	if err != nil {
		fmt.Println("err: ", err.Error())
	}
}

func DeletePage() {
	page := Page{Id: 2}
	o := orm.NewOrm()
	_, err := o.Delete(&page)

	if err != nil {
		fmt.Println("err: ", err.Error())
	}
}
